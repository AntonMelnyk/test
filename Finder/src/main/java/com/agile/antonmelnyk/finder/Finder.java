package com.agile.antonmelnyk.finder;

import com.agile.antonmelnyk.finder.entity.ParamEntity;
import com.agile.antonmelnyk.finder.processor.Processor;
import com.agile.antonmelnyk.finder.processor.impl.ProcessorImpl;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class Finder {

    private static final Logger LOGGER = LoggerFactory.getLogger(Finder.class);

    public static final String TEMPLATE_ID = "make-everything-ok-button";

    private Processor<String, ParamEntity> processor;

    private Finder () {
        this.processor = new ProcessorImpl();
    }

    public Processor<String, ParamEntity> getProcessor() {
        return processor;
    }

    public static void main(String[] args) {

        if (args.length < 2) {
            throw new IllegalArgumentException();
        }

        String path = new Finder().getProcessor()
                .process(new ParamEntity(args[0], args[1], TEMPLATE_ID));
        System.out.println(path);
        LOGGER.info("Path :: {}", path);
    }
}
