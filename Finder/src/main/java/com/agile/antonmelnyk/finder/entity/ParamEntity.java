package com.agile.antonmelnyk.finder.entity;

import java.util.Objects;

public class ParamEntity {

    private final String _template;
    private final String _checkedFile;
    private final String _originID;

    public ParamEntity (final String template,
                        final String chekedFile,
                        final String originID) {

        _template = Objects.requireNonNull(template, "Template can't be null");
        _checkedFile = Objects.requireNonNull(chekedFile, "Checked file can't be null ");
        _originID = Objects.requireNonNull(originID, "Origin id can't be null");
    }

    public String get_template() {
        return _template;
    }

    public String get_checkedFile() {
        return _checkedFile;
    }

    public String get_originID() {
        return _originID;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        ParamEntity that = (ParamEntity) o;
        return Objects.equals(_template, that._template) &&
                Objects.equals(_checkedFile, that._checkedFile) &&
                Objects.equals(_originID, that._originID);
    }

    @Override
    public int hashCode() {
        return Objects.hash(_template, _checkedFile, _originID);
    }

    @Override
    public String toString() {
        return "ParamEntity{" +
                "_template='" + _template + '\'' +
                ", _checkedFile='" + _checkedFile + '\'' +
                ", _originID='" + _originID + '\'' +
                '}';
    }
}
