package com.agile.antonmelnyk.finder.processor;

@FunctionalInterface
public interface Processor<T, E> {

    T process(E source);
}
