package com.agile.antonmelnyk.finder.processor.impl;

import com.agile.antonmelnyk.finder.entity.ParamEntity;
import com.agile.antonmelnyk.finder.processor.Processor;
import com.agile.antonmelnyk.finder.search.SearchService;
import com.agile.antonmelnyk.finder.search.impl.SearchServiceImpl;
import org.jsoup.nodes.Element;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.util.*;
import java.util.function.Function;
import java.util.stream.Collectors;

import static java.util.stream.Collectors.counting;

public class ProcessorImpl implements Processor<String, ParamEntity> {

    private static final Logger LOGGER = LoggerFactory.getLogger(ProcessorImpl.class);

    private final SearchService  searchService;

    public ProcessorImpl() {
        this.searchService = new SearchServiceImpl();
    }

    @Override
    public String process(final ParamEntity source) {

        File fileToCheck = new File(source.get_checkedFile());
        Optional<Element> button = searchService.findById(new File(source.get_template()), source.get_originID());

        LOGGER.debug("Button : {}", button);

        List<String> cssQueries = createCssQuery(button.orElseThrow(NotFoundException::new));

        Set<Element> elements = new HashSet<>();

        Map<String, Long> matcherMap = cssQueries.stream()
                .flatMap(query -> searchService.fyndByCSSQuery(fileToCheck, query)
                        .orElseThrow(NotFoundException::new)
                        .stream())
                .filter(element -> element.tag().equals(button.get().tag()))
                .peek(elements::add)
                .map(Element::toString)
                .collect(Collectors.groupingBy(Function.identity(), counting()));

        if (matcherMap.isEmpty()) {
            throw new NotFoundException("No element was find");
        }

        String maxMatched = Collections.max(matcherMap.entrySet(), Comparator.comparingLong(Map.Entry::getValue)).getKey();

        Optional<Element> finalElement = elements.stream()
                .filter(element -> maxMatched.equals(element.toString())).findFirst();

        return showPath(finalElement.orElseThrow(NotFoundException::new));
    }

    private List<String> createCssQuery(Element element) {

        List<String> cssQueries = element.attributes().asList().stream()
                .filter(attribute -> !"id".equalsIgnoreCase(attribute.getKey()))
                .map(attribute -> String.format("%s[%s=\"%s\"]", element.tagName(),
                        attribute.getKey(),
                        attribute.getValue()))
                .collect(Collectors.toList());

        return cssQueries;
    }

    private String showPath(Element element) {
        return element.parents().stream()
                .collect(Collectors.collectingAndThen(
                        Collectors.toCollection(ArrayList::new), lst -> {
                            Collections.reverse(lst);
                            return lst.stream();
                        }
                ))
                .map(Element::tagName)
                .collect(Collectors.joining("  > ")).concat(" > " + element.toString());
    }

    private static class NotFoundException extends RuntimeException {

        public NotFoundException () {
            super();
        }

        public NotFoundException (String message) {
            super(message);
        }
    }
}
