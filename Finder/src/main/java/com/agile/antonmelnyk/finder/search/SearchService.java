package com.agile.antonmelnyk.finder.search;

import org.jsoup.select.Elements;
import java.io.File;
import java.util.Optional;

public interface SearchService {

    String CHARSET = "utf8";

    Optional findById (File source, String query);

    Optional<Elements> fyndByCSSQuery (File source, String elementId);
}
