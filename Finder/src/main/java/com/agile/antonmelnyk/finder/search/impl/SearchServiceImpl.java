package com.agile.antonmelnyk.finder.search.impl;

import com.agile.antonmelnyk.finder.search.SearchService;
import org.jsoup.Jsoup;
import org.jsoup.nodes.Document;
import org.jsoup.nodes.Element;
import org.jsoup.select.Elements;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

import java.io.File;
import java.io.IOException;
import java.util.Optional;

public class SearchServiceImpl implements SearchService {

    private static final Logger LOGGER = LoggerFactory.getLogger(SearchServiceImpl.class);

    @Override
    public Optional<Element> findById(File source, String elementId) {
        try {
            final Document doc = Jsoup.parse(
                    source,
                    CHARSET,
                    source.getAbsolutePath());

            return Optional.ofNullable(doc.getElementById(elementId));

        } catch (IOException e) {
            LOGGER.error("Error reading [{}] file", source.getAbsolutePath(), e);
            return Optional.empty();
        }
    }

    @Override
    public Optional<Elements> fyndByCSSQuery(File source, String query) {
        try {
            final Document doc = Jsoup.parse(
                    source,
                    CHARSET,
                    source.getAbsolutePath());

            return Optional.of(doc.select(query));

        }
        catch (IOException e) {
            LOGGER.error("Error reading [{}] file", source.getAbsolutePath(), e);
            return Optional.empty();
        }
    }
}
