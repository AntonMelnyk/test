# Test Beck-End

Author: Melnyk Anton

How to test:

* go to app folder
* execute ./gradlew clean assemble
* go to 'build' folder
* run java -cp <path_to_jar>.com.agile.task-1.0-SNAPSHOT.jar com.agile.antonmelnyk.finder.Finder <path_to_template> <path_to_compared_file>
